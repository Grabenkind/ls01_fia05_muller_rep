package de.steven.konsolenausgabe;

public class Aufgabe3 {
	
	public static void main(String[] args) {
		String f = "Fahrenheit";
		String c = "Celsius";
		int[] left = {-20, -10, 0, 20, 30};
		double[] right = {-28.8889, -23.3333, -17.7778, -6.6667, -1.1111};
		
		System.out.printf("%-13s" + "|" + "%13s\n", f, c);
		System.out.printf("---------------------------\n");
		
		System.out.printf("%-13d", left[0]);
		System.out.printf("|");
		System.out.printf("%13f\n", right[0]);
		
		System.out.printf("%-13d", left[1]);
		System.out.printf("|");
		System.out.printf("%13f\n", right[1]);
		
		System.out.printf("%-13d", left[2]);
		System.out.printf("|");
		System.out.printf("%13f\n", right[2]);
		
		System.out.printf("%-13d", left[3]);
		System.out.printf("|");
		System.out.printf("%13f\n", right[3]);
		
		System.out.printf("%-13d", left[4]);
		System.out.printf("|");
		System.out.printf("%13f\n", right[4]);
	}

}
