package de.steven.konsolenausgabe;


public class Fakultät {

	public static void main(String[] args) {
		String[] n_left = {"0!", "1!", "2!", "3!", "4!", "5!"};
		int[] n_right = {1, 1, 2, 6, 24, 120};
		String equals = "=";
		
		System.out.printf("%-20s" + "%-10s" + "%20s" + "%20d\n", n_left[0], equals, equals, n_right[0]);
		System.out.printf("%-20s" + "%-10s" + "1" + "%19s" + "%20d\n", n_left[1], equals, equals, n_right[1]);
		System.out.printf("%-20s" + "%-10s" + "1 * 2" + "%15s" + "%20d\n", n_left[2], equals, equals, n_right[2]);
		System.out.printf("%-20s" + "%-10s" + "1 * 2 * 3" + "%11s" + "%20d\n", n_left[3], equals, equals, n_right[3]);
		System.out.printf("%-20s" + "%-10s" + "1 * 2 * 3 * 4" + "%7s" + "%20d\n", n_left[4], equals, equals, n_right[4]);
		System.out.printf("%-20s" + "%-10s" + "1 * 2 * 3 * 4 * 5" + "%3s" + "%20d\n", n_left[5], equals, equals, n_right[5]);
	}

}
